import axios, { AxiosRequestConfig, AxiosInstance } from "axios";
// import store from 'store';

export const createApi = (config?: AxiosRequestConfig): AxiosInstance => {
    const api = axios.create(config);

    // Add a request interceptor
    api.interceptors.request.use(
        config => {
            // const auth = store.get('auth');
            // if (auth && auth.token) {
            //     config.headers['Authorization'] = 'Bearer ' + auth.token;
            // }
            return config;
        },
        error => {
            Promise.reject(error)
        });
    return api;
}
export default createApi;