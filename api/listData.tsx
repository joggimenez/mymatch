import createApi from ".";

const mgpApi = createApi({
    //TODO: configure API
    baseURL: 'https://reqres.in/api/'
});

export const getAllUsers = async () => {
    const usersResponse = await mgpApi.get('users');
    let normalized = null;
    if (usersResponse.status === 200) {
        normalized = usersResponse.data.data;
    }
    return normalized;
};